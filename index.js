const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const PORT = process.env.PORT || 3000;


let arr = [];
// Middleware to parse JSON body
app.use(bodyParser.json());

// Endpoint to receive webhook events from Intercom
app.post("/intercom/webhook", (req, res) => {
  const event = req.body;

arr.push(event)
  // You can perform actions based on the type of event received
  res.json(("Received Intercom event:", event));

});

app.get("/",(req,res)=>{
  res.json(arr)
})

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
